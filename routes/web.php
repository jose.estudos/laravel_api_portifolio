<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('formulario');
});
Route::post('/autenticacao', 'App\Http\Controllers\UsuarioController@autenticar');
Route::post('/cadastraUsuario', 'App\Http\Controllers\UsuarioController@cadastarUsuario');

Route::get('/usuarios', 'App\Http\Controllers\UsuarioController@listar');