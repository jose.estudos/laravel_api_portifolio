<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">

<title>QUAESTUM</title>
<link rel="stylesheet" type="text/css" href="../semantic.css">

</head>
<body>
  
	<form class="ui form" action="/autenticacao" method="post">
		    @csrf

		
		<div class="field">
			<label>CPF</label> <input type="text" name="cpf"
				placeholder="usuario cpf">
		</div>
		<div class="field">
			<label>Senha</label> <input type="Password" name="senha"
				placeholder="Senha">
		</div>
			
		<button class="ui button" type="submit">Submit</button>
	</form>
</body>
</html>
