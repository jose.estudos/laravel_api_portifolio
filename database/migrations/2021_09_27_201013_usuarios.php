<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Usuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->string('nome_usuario');
            $table->string('cpf_usuario');
            $table->string('telefone_usuario');
            $table->string('senha_usuario');
            $table->date('dt_nasc_usuario')->nullable();
            $table->dateTime('dt_hr_registro_usuario')->nullable();
            $table->bigInteger('id_dependentes')->unsigned()->index(); 
            $table->foreign('id_dependentes')->references('id')->on('dependentes')->onDelete('cascade');
        });
           
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
