<?php
namespace App\api;

use App\Models\Usuarios;
use Illuminate\Support\Facades\DB;

class UsuariosAPI
{

    function listar()
    {
        return $users = DB::table('usuarios')->join('dependentes', 'dependentes.id', '=', 'usuarios.id_dependentes')->
        get();
    }

    function salvar($usuario)
    {
        
        
        $valido = $this->verificaDuplicidade($usuario);
        if ($valido) {
            return 'usuario ja Cadastarado';
        } else {
            return 'cadastro realizado com sucesso';
        }
    }
    

    function verificaDuplicidade($usuario)
    {
        $usuarios =     DB::table('usuarios')
        ->where('cpf_usuario', '=', $usuario)
        ->get();
        
        foreach ($usuarios as $user) {
            if($user->cpf_usuario  == $usuario  ) {
                return true;
            }else {
                return  false;
            }
        }
    }
}

