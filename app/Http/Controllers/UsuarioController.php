<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuarios;
use App\api\Autenticacao;
use function GuzzleHttp\json_encode;
use App\api\UsuariosAPI;

class UsuarioController extends Controller
{

    function autenticar()
    {
        $cpf = request('cpf', 0);
        $senha = request('senha', 0);

        $autentica = new Autenticacao();
        $autenticado = $autentica->autenticar($cpf, $senha);

        return json_encode($autenticado);
    }

    function listar()
    {
        $usuarios = new UsuariosAPI();
        return json_encode($usuarios->listar());
    }

    function cadastarUsuario()
    {
        $usuarios = new UsuariosAPI();

        $cpf_usuario = request('cpf', 0);
        $senha =sha1( request('senha', 0));
        $telefone_usuario = request('telefone', 0);
        $dt_nasc_usuario = request('dt_nasc_usuario', 0);
        $dt_hr_registro_usuario = request('dt_hr_registro_usuario', 0);
        
        $id_dependente = request('id_dependente', 0);
        
        $usuario =  new Usuarios();
        $usuario->cpf_usuario = $cpf_usuario;
        $usuario->senha = $senha;
        $usuario->telefone_usuario = $telefone_usuario;
        $usuario->dt_nasc_usuario = $dt_nasc_usuario;
        $usuario->dt_hr_registro_usuario = $dt_hr_registro_usuario;
        $usuario->id_dependente = $id_dependente;
        
        
        $retorno = $usuarios->salvar($usuario);

        return json_encode($retorno);
    }
}
